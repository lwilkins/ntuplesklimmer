import ROOT as r
import sys, os
import glob
import re
import json


def load_functions(function_list):
    try:
        for func in function_list:
            r.gROOT.ProcessLineSync(".L " + func + "+")
    except:
        print("No functions defined.")


def load_config(config_file_name):
    try:
        with open(config_file_name) as config_file:
            config = json.load(config_file)
    except:
        print("Invalid config name! Please enter a valid config file name")
        sys.exit()

    return config


def rootify_branch_list(branch_list):
    r_branch_list = r.vector("string")()
    [r_branch_list.push_back(str(b)) for b in branch_list]

    return r_branch_list


def get_file_size(path):
    size = 0
    for file in glob.glob(path):
        size += os.path.getsize(file)

    return size


def print_list(list_to_print):
    for l in list_to_print:
        print(l)


def build_list_from_regex(tree, regex):
    branch_list = []
    branches = tree.GetColumnNames()
    for branch in branches:
        if re.search(regex, branch):
            branch_list.append(branch)

    return branch_list


def prepare_branch_list(tree, branch_list):
    if branch_list[0] == "*":
        return [None]
    final_branch_list = list(branch_list)
    for branch in branch_list:
        if "*" in branch:
            branch_regex = branch.replace("*", ".*")
            branch_regex = "^" + branch_regex
            branch_regex = branch_regex + "$"
            regex_branches = build_list_from_regex(tree, branch_regex)
            final_branch_list.remove(branch)
            final_branch_list += regex_branches

    r_final_branch_list = rootify_branch_list(final_branch_list)

    return r_final_branch_list


def define_variables(df, new_vars):
    if not new_vars[0] is None:
        for i, var in enumerate(new_vars):
            df = df.Define(str(var[0]), str(var[1]))
    return df


def set_batch():
    r.gROOT.SetBatch(True)


def set_mt(n_thread):
    r.ROOT.EnableImplicitMT(n_thread)


class Config(object):
    def __init__(self, config_file_name):
        self.config_file_name = config_file_name
        self.config_dict = self._load_config(self.config_file_name)
        self.cuts = str(self._get_key("cuts"))
        self.new_vars = self._get_key("new_vars")
        self.branch_list = self._get_key("branches")
        self.functions = self._get_key("functions")

    def _load_config(self, config_file_name):
        try:
            with open(config_file_name) as config_file:
                config = json.load(config_file)
        except:
            print("Invalid config name! Please enter a valid config file name")
            sys.exit()

        return config

    def _get_key(self, key):
        if key in self.config_dict:
            return self.config_dict[key]
        else:
            print("No {0} specified.".format(key))
            return None

