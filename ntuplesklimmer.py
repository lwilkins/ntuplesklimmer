import time
from optparse import OptionParser

import ROOT as r
from utils import *


def get_options():
    parser = OptionParser()

    parser.add_option("-f", "--file", dest="file_name", help="file name")
    parser.add_option("-t", "--tree", dest="tree_name", help="tree name")
    parser.add_option(
        "-o", "--output", dest="output_file_name", help="Output file name"
    )
    parser.add_option(
        "-c", "--config", dest="config_file_name", help="Config file name", default=None
    )
    parser.add_option("-e", "--events", dest="n_events", help="n_events", default=0)
    parser.add_option("-m", "--mthread", dest="mthread", help="Set number of threads" default=0)
    (options, args) = parser.parse_args()

    return options


if __name__ == "__main__":

    options = get_options()
    config = Config(options.config_file_name)
    load_functions(config.functions)
    if options.mthread:
        set_mt(options.mthread)

    print("Sklimming {0}".format(options.file_name))
    print("Applying cut {0}".format(config.cuts))
    print("Branches to keep:")
    print_list(config.branch_list)

    start = time.time()

    df = r.RDataFrame(options.tree_name, options.file_name)

    #Run over a subset of events
    if options.n_events > 0:
        r.ROOT.DisableImplicitMT()  # Cannot run over range in MT
        df = df.Range(int(options.n_events))

    #Create any new variables
    if config.new_vars:
        df = define_variables(df, config.new_vars)

    #Apply any cuts
    if config.cuts: 
        df = df.Filter(config.cuts)

    r_branch_list = prepare_branch_list(df, config.branch_list) #Convert list to ROOT vector
    if r_branch_list[0]:
        df.Snapshot(options.tree_name, options.output_file_name, r_branch_list)
    else:
        df.Snapshot(options.tree_name, options.output_file_name)
        
    original_size = get_file_size(options.file_name)
    new_size = get_file_size(options.output_file_name)
    end = time.time()
    print(
        "Original size was {0:.1f}mb, new file is {1:.1f}mb. Reduction of {2:.3f}%.".format(
            original_size / 1e6,
            new_size / 1e6,
            100 * (1 - (float(new_size) / original_size)),
        )
    )
    print("time taken {0}s".format(end - start))
