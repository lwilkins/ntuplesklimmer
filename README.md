# NtupleSklimmer

This is a small python script which allows you to easily slim and skim an ntuple. It is based on ROOT's new `RDataFrame` which can multi-thread the event loop.

I have been testing in `root-6.16.00` but it also seems to work fine in `root-6.14.04`. To setup, all that is needed is:
```
setupATLAS
lsetup "root 6.16.00-x86_64-slc6-gcc62-opt"
```
The script itself takes 4 arguments:

| **Option** | **Usage** |
| ---------- | ---------- |
| `-t` | Name of the tree you want to loop over. The output tree will also have this name. |
| `-f` | The path and name of input file. Wildcards can also be used but make sure to put quote marks around path if doing so. |
| `-o` | Path and name of output file. |
| `-c` | Path and name of config file (json).|
| `-e` | Number of events to run over. (Set to 0 by default which runs over all events) |

The config file is a json in a separate file, `ntuplesklimmer_config.json   ` is an example. The config has the following options:

| **Option** | **Usage** |
| ---------- | ---------- |
| `branches` | A list of the branches you wish to keep in both reco and particle trees. Can use wildcards, e.g. for btag systeamtics `weight_btagSF*`. |
| `reco_cuts` | String with the cuts to apply. |
| `new_vars` | New  variables to be created. This is a list of 2 entry lists, one entry for the name of the new variable and one for the expression to create it. |
| `functions` | List of the ROOT macros needed for the cuts or new variables. |



